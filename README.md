# N1QL Browser

## Usage
You can compile this utility or alternatively (recommended) just use the
[Docker image](https://hub.docker.com/r/elfido/n1qlbrowser/).

Notice that the configuration file in this example is mapped to a file mounted through the host system.

```sh
docker run -d  -v /user/config:/app/config -p 8080:8080 elfido/n1qlbrowser:latest
```

Store as many servers as required in the configuration in the file ```config/config.json```,
use the following format:

```javascript
[
	{
		"name": "<server_name>",
		"url": "<server_ip>",
		"user": "<server_user>",
		"password": "<server_password>",
		"buckets": [
			{"bucket": "<protected_bucket_name>", "password": "<protected_bucket_password>"}
		]
	}
]
```

Notice that the configuration is an array, while the servers along with their user name and password
are required, the list of buckets is optional, they will be discovered automatically. You should only
list those buckets that are password protected.

You can use an environment variable called *N1QL_SERVERS* instead to pass the array of servers (in the
same json format). The environment variable approach will work the same if you run the application directly
in your server or if you use a Docker container.

## Developers
Framework
* Go 1.6
* NodeJS for front end (grunt and bower required)

### Building UI

From the ```/ui``` folder, execute:
```
bower install
npm install
grunt build
```

### Modifying the UI

Every time you modify the UI (pug) files you will need to recompiled them with ```grunt build```; for
convenience, I added a ```grunt watch``` or simply ```grunt``` (since is the default task) that will 
build automatically every time you save a change.