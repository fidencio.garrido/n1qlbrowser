/* global $, APIClient, APIApp, Vue, APIHtml, APIMap, _ */
const webapp = new Vue({
	el: "#app",
	data: {
		appstatus: "",
		browser: {
			editPage: false,
			currentPage: 0,
			pageSize: 100,
			pages: 0,
			selectedItem: null,
			total: 0,
			totalPages: 0,
			results: []
		},
		bucket: null,
		buckets: [],
		displayExplain: false,
		displayMode: "json",
		explainPlan: "",
		filteredHistory: [],
		geolat: "",
		geolon: "",
		mainModule: "",
		query: "",
		results: [],
		resultsHeaders: [],
		resultsJson: [],
		saveQueries: true,
		server: null,
		servers: null,
		showEditorTools: true,
		showMenu: true,
		showStats: true,
		schema: {},
		stats: {
			averageitem: 0,
			executionTime: 0,
			indexesUsed: "",
			latency: 0,
			resultCount: 0,
			resultSize: 0,
			totalTime: 0
		}
	},
	created() {
		this.refreshServers();
	},
	filters: {
		localize(n) {
			return n.toLocaleString();
		},
		niceBoolean(value) {
			if (value === true) {
				return "On";
			}
			else {
				return "Off";
			}
		},
		toKb(bytes) {
			let kb = bytes / 1024;
			return kb.toLocaleString();
		},
		toMb(bytes) {
			let mb = bytes / 1024 / 1024;
			return mb.toLocaleString();
		},
		toMs(nano) {
			let ms = nano / 1000000;
			return ms.toLocaleString();
		}
	},
	methods: {
		clearStats() {
			this.stats = {
				indexesUsed: "",
				resultCount: 0,
				resultSize: 0,
				averageitem: 0,
				latency: 0,
				executionTime: 0,
				totalTime: 0
			}
			this.results = "";
			this.resultsHeaders = [];
			this.resultsJson = [];
		},
		buildMap(){
			this.displayMode="map";
			setTimeout(()=>{
				APIMap.build();
			}, 100);
			const history = APIApp.getGeoReferences(this.bucket),
				ndx = (history.next-1 >= 0) ? history.next-1 : 0,
				last = (history.history && history.history[ ndx ]) || null;
			if (last){
				if (last.lat && last.lon){
					this.geolat = last.lat;
					this.geolon = last.lon;
					let self = this;
					setTimeout(()=>{
						this.mapResults();
					}, 200);
				}
			}
		},
		connect(e) {
			e.preventDefault();
			this.server = $(".server").val();
			console.log("connecting to", this.server);
			this.getBuckets(this.server);
		},
		discoverSchema(){
			if (this.resultsJson.length>0){
				this.displayMode = "schema";
				this.schema = jsonUtils.findSchema(this.resultsJson, 2);
			}
		},
		displayDocument(id, document){
			this.browser.selectedItem = {
				id: id,
				item: document
			}
		},
		execute() {
			const self = this;
			this.clearStats();
			this.filteredHistory = [];
			this.appstatus = "Executing Query";
			this.explain(this.query);
			APIClient.execute(this.server, this.bucket, this.query).then((results) => {
				self.stats.executionTime = results.executionTime;
				self.stats.totalTime = results.totalTime;
				self.stats.resultCount = results.resultCount;
				self.stats.resultSize = results.resultSize;
				self.stats.averageitem = results.resultSize / results.resultCount;
				let latency = self.stats.totalTime - (self.stats.executionTime / 1000000);
				self.stats.latency = latency.toLocaleString();
				self.results = JSON.stringify(results.results, null, "  ");
				self.getHeaders(results.results);
				this.appstatus = "";
				self.refreshResults();
				if (self.saveQueries === true)
					APIApp.saveQuery(self.bucket, self.query, "Success", results.executionTime);
			}).catch((error) => {
				const e = (typeof error === "object" && error.responseText) ? error.responseText : error;
				self.appstatus = "Error: " + e;
				if (self.saveQueries === true)
					APIApp.saveQuery(self.bucket, self.query, "Error", e);
			});
		},
		explain(query) {
			const self = this;
			if (query.toLowerCase().trim().indexOf("explain") !== 0) {
				const equery = `explain ${query}`;
				APIClient.execute(this.server, this.bucket, equery).then((explain) => {
					if (typeof explain.results === "object" && Array.isArray(explain.results)) {
						const plan = explain.results[0].plan;
						let indexes = APIApp.extractIndexes(plan);
						self.stats.indexesUsed = indexes.join(",");
						self.explainPlan = JSON.stringify(plan, null, "  ");
					}
				});
			}
		},
		filterHistory() {
			const queries = APIApp.getQueries(this.bucket);
			if (this.saveQueries && Array.isArray(queries.history)) {
				let query = $(".query").val().toLowerCase(),
					filtered = queries.history.filter((entry) => {
						if (entry && (typeof entry.q!=="undefined")){
							return entry.q.toLowerCase().indexOf(query) >= 0;
						}
					}).slice(0, 10);
				this.filteredHistory = filtered;
			}
			else {
				this.filteredHistory = [];
			}
		},
		fontSize(direction){
			APIHtml.fontSize(".n1ql", direction);
		},
		format(query){
              const words = query.trim().split(" "),
                keywords = ["select", "insert", "update", "delete", "explain", "from", "limit", "offset", "join", "inner", "count", "where", "using", "and", "or", "group", "by", "having","order", "create", "primary", "index", "on"];
              let output = "";
              words.forEach((word)=>{
                if (keywords.includes(word)){
                  output+=`<span class="text-primary">${word} </span>`;
                } else{
                  output+=`${word} `;
                }
              });
              return output;
            }, 
		getBuckets(server) {
			const self = this;
			this.appstatus = "Connecting to server";
			APIClient.getBuckets(server).then((buckets) => {
				if (buckets && buckets.length > 0){
					self.buckets = buckets;
					self.appstatus = "";
				} else {
					self.servers.forEach((s) => {
						if (server === s.name) {
							const buckets = [];
							s.buckets.forEach((b) => {
								console.log(b);
								buckets.push({
									Name: b.bucket,
									Quota: 0,
									Replicas: 0,
								});
							});
							self.buckets = buckets;
						}
					});
				}
			}).catch((error) => {
				console.log(error);
				let e = (typeof error === "object") ? JSON.parse(error) : error;
				self.appstatus = `ERROR: ${e}`;
			});
		},
		getHeaders(results){
			let h = [],
				r = [];
			const self = this;
			if (Array.isArray(results)){
				results.forEach( (row)=>{
					const item = (typeof row[self.bucket] === "undefined") ? row : row[self.bucket];
				r.push(item)
				const keys = Object.keys(item);
				keys.forEach((k)=>{
					if (!h.includes(k)){
					h.push(k);
				}
			});
			});
				this.resultsHeaders = h;
				this.resultsJson = r;
			}
		},
		mapResults(){
			const r = JSON.parse(this.results),
				latField = this.geolat,
				lonField = this.geolon,
				geoPoints = [];
			if (Array.isArray(r)){
				APIApp.saveGeoReference(this.bucket, this.query, latField, lonField);
				r.forEach( (item)=>{
					const keys = (typeof item === "object" && item!==null) ? Object.keys(item) : null;
					if (keys){
						const record = (keys.length === 1 && keys[0] == this.bucket ) ? item[keys[0]] : item;
						let _lat = _.get(record, latField),
							_lon = _.get(record, lonField);
						if (typeof _lat !== "undefined" && _lon !== "undefined"){
							geoPoints.push( {lat: _lat, lon: _lon, item: record} );
						}
					}
				});
				APIMap.map(geoPoints);
			} else{
				console.log("Results can't be mapped");
				console.log(r);
			}
		},
		openClassicView(bucket){
			const self = this;
			this.bucket = bucket;
			this.mainModule = "documentbrowser";
			const queryCount = `select count(*) as total from ${this.bucket}`;
			this.queryBrowser(0);
			APIClient.execute(this.server, this.bucket, queryCount).then((results) => {
				const total = results.results[0].total;
				let pages = Math.floor(total / self.browser.pageSize);
				if ( (pages*self.browser.pageSize) < total){
					pages++;
				}
				self.browser.totalPages = pages;
				self.browser.total = total;
			});
		},
		openEditor(bucket) {
			this.bucket = bucket;
			this.appstatus = "";
			const self = this;
			APIClient.bucketConnect(this.server, this.bucket).then((resp) => {
				self.showMenu = false;
				self.mainModule = "query";
				self.query = `select * from \`${bucket}\` limit 10;`;
			}).catch((error)=>{
				const e = (typeof error === "object" && error.responseText) ? error.responseText : error;
				self.appstatus = "Error: " + e;
			});
		},
		queryBrowser(page){
			let offset = page*this.browser.pageSize;
			this.browser.currentPage = page;
			this.query = `select meta().id as id, ${this.bucket}.* from ${this.bucket}  limit ${this.browser.pageSize} offset ${offset}`;
			this.browser.results = [];
			APIClient.execute(this.server, this.bucket, this.query).then((results) => {
				const r = [];
				results.results.forEach((res)=>{
					const id = res.id;
					delete res.id;
					const item = {
						id: id,
						item: res
					}
					r.push(item);
				});
				this.browser.results = r;
			});
		},
		refreshResults(){
			const mode = this.displayMode;
			switch(mode){
				case "map":
						this.buildMap();
					break;
				default:
					console.log(mode);
			}
		},
		refreshServers() {
			let self = this;
			this.appstatus = "Loading servers";
			APIClient.getServers().then((_servers) => {
				this.appstatus = "";
				self.servers = _servers;
			}).catch((err) => {
				alert(err);
			});
		},
		tablefy(json){
			const jsonType = (typeof json).toUpperCase();
			return APIHtml.tx[jsonType](json);
		},
		toggleDisplayMode(){
			if (this.displayMode === "json"){
				this.displayMode = "table";
			} else{
				this.displayMode = "json";
			}
		},
		updateCurrentPage(e){
			if (e.keyCode==13){
				const val = document.getElementById("curPage").value;
				if (!isNaN(val) && val>=0 && val < this.browser.totalPages){
					this.queryBrowser(val);
				} else{
					alert("Invalid input");
				}
				this.browser.editPage=false;
			}
		}
	}
});