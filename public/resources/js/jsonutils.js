const jsonUtils = {
	_guessType(value){
		const t = typeof value;
		return t;
	},
	_itemSchema(item, parent, fullpath){
		const keys = Object.keys(item).sort(),
			schema = {};
		schema.keys = [];
		let childrenSchema = [];
		keys.forEach((k)=>{
			let _field = {parent: parent, fpath: fullpath, name: k, type: jsonUtils._guessType(item[k])};
			if (_field.type === "object" && !Array.isArray(item[k])){
				let pre = parent ? `${parent}` : "";
				childrenSchema = jsonUtils._itemSchema(item[k], k, pre);
			} else{
				if (Array.isArray(item[k])){
					_field.type = "array";
				}
			}
			schema.keys.push(_field);
			if (childrenSchema.keys.length>0){
				schema.keys = schema.keys.concat(childrenSchema.keys);
			}
		});
		return schema;
	},
	merge(source, newschema, maxKeys){
		newschema.forEach((s)=>{
			let pre = s.parent === "" ? "" : `${s.parent}.`,
				composed = `${pre}${s.name}`;
			if (typeof source[composed] === "undefined"){
				source[composed] = s;
				s.isMandatory = false;
				source[composed].count = 0;
			}
			source[composed].count++;
			if (source[composed].count === maxKeys){
				source[composed].isMandatory = true;
			}
		});
		return source;
	},
	getNodes(obj, parent){
		let attributes = [];
		const _schemas = jsonUtils._itemSchema(obj).keys;
		const master = {};
		_schemas.forEach((s)=>{
			let node = {
				name: s.name,
				type: s.type,
				count: 1,
				isMandatory: false,
				parent: parent,
				children: (s.schema) ? s.schema.keys : null
			};
			if (typeof master[s.name] !== "undefined"){
				node.count = ++master[s.name].count;
				if (node.count === limit){
					node.isMandatory = true;
				}
			}
			master[s.name] = node;
			attributes.push(master[s.name]);
			if (s.schema){
				attributes = attributes.concat( jsonUtils.getNodes(s.schema.keys, s.name) );
			}
		});
		return attributes;
	},
	findSchema(jsonSet, maxItems){
		let schemas = [],
			master = {keyscount: {}},
			firstLevel = {};
		let attributes = [];
		if (jsonSet && Array.isArray(jsonSet) && jsonSet.length>0){
			const limit = ( maxItems && (maxItems > jsonSet.length)) ? jsonSet.length : maxItems;
			for (let i=0; i<limit; i++){
				const schema = jsonUtils._itemSchema(jsonSet[i], "", "");
				master = jsonUtils.merge(master, schema.keys, limit);
			}
		}
		return master;
	}
};

/*

 Product:
	 type: object
	 properties:
		 product_id:
			 type: string
			 description: Unique identifier representing a specific product for a given latitude & longitude. For example, uberX in San Francisco will have a different product_id than uberX in Los Angeles.
		 description:
			 type: string
			 description: Description of product.
		 display_name:
			 type: string
			 description: Display name of product.
		 capacity:
			 type: string
			 description: Capacity of product. For example, 4 people.
		 image:
			 type: string
			 description: Image URL representing the product.

 */