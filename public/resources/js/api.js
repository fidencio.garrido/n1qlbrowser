/* global $, localStorage, google */
const APIClient = {
	execute(server, bucket, query){
		const api = `/query/${server}/${bucket}`,
			payload = {
				q: query
			};
		return new Promise( (resolve, reject)=>{
			const start = new Date();
			$.ajax({
				url: api,
				method: "post",
				data: JSON.stringify(payload),
				success(results){
					let end = new Date() - start;
					results.totalTime = end;
					let _res = [];
					if (results.results === null){
						results.results = [];
					}
					results.results.forEach( (r)=>{
						_res.push( JSON.parse(r) );
					});
					results.results = _res;
					resolve(results);
				},
				error(err){
					reject(err);
				}
			});
		});
	},
	bucketConnect(server, bucket){
		const api = `/connection/${server}/${bucket}`;
		return new Promise( (resolve, reject)=>{
			$.ajax({
				url: api,
				method: "post",
				success(response){
					resolve(response);
				},
				error(err){
					reject(err);
				}
			});
		});
	},
	getBuckets(server){
		const api = `/connection/${server}`;
		return new Promise( (resolve, reject)=>{
			$.ajax({
				url: api,
				method: "post",
				success(buckets){
					resolve( JSON.parse(buckets) );
				},
				error(err){
					reject(err.statusText);
				}
			});
		});
	},
	getServers(){
		const api = "/server";
		return new Promise( (resolve, reject)=>{
			$.ajax({
				url: api,
				contentType: "json",
				success(servers){
					resolve( JSON.parse(servers) );
				},
				error(err){
					reject(err);
				}
			});
		});
	}
};

const APIApp = {
	maxQueries: 100,
	maxGeoLocation: 100,
	canSave(){
		return typeof(Storage) !== "undefined";
	},
	extractIndexes(plan){
		const keys = Object.keys(plan);
		let _indexes = [];
		keys.forEach((k)=>{
			if (k.indexOf("__")<0){
				if (typeof plan[k] === "object" && !Array.isArray(plan[k])){
					let t = APIApp.extractIndexes(plan[k]);
					t.forEach( (_t)=>{
						if (!_indexes.includes(_t)){
							_indexes.push(_t);
						}	
					});
				} else{
					if ( Array.isArray(plan[k]) ){
						plan[k].forEach( (element)=>{
							let t = APIApp.extractIndexes(plan[k]);
							t.forEach( (_t)=>{
								if (!_indexes.includes(_t)){
									_indexes.push(_t);
								}	
							});
						});
					} else{
						if (k==="index"){
							if (!_indexes.includes(k)){
								_indexes.push(plan[k]);
							}
						}
					}
				}
			}
		});
		return _indexes;
	},
	saveGeoReference(bucket, query, latField, lonField){
		if (APIApp.canSave()){
			const id = `geo-${bucket}`;
			let items = APIApp.getGeoReferences(bucket);
			if (items.next >= APIApp.maxQueries){
				items.next = 0;
			}
			items.history[items.next] = {q: query, lat: latField, lon: lonField };
			items.next += 1;
			localStorage.setItem(id, JSON.stringify(items));
		}
	},
	saveQuery(bucket, query, status, code){
		if (APIApp.canSave()){
			let items = APIApp.getQueries(bucket);
			if (items.next >= APIApp.maxQueries){
				items.next = 0;
			}
			items.history[items.next] = {q: query, status: status, code: code};
			items.next += 1;
			localStorage.setItem(bucket, JSON.stringify(items));
		}
	},
	getGeoReferences(bucket){
		if (APIApp.canSave()){
			const id = `geo-${bucket}`;
			let items = localStorage.getItem(id) || { next: 0, history: [] };
			if (typeof items === "string"){
				items = JSON.parse(items);
			}
			return items;
		} else{
			return null;
		}
	},
	getQueries(bucket){
		if (APIApp.canSave()){
			let items = localStorage.getItem(bucket) || {next: 0, history: []};
			if (typeof items === "string"){
				items = JSON.parse(items);
			}
			return items;
		} else{
			return null;
		}
	}
};

const APIHtml = {
	fontSize(selector, direction){
		let size = $(selector).css("fontSize");
		if (size.indexOf("px")>0){
			size = parseInt(size.slice(0, size.length-2));
		}
		if (direction === "up"){
			++size;
		} else{
			--size;
		}
		$(selector).css("fontSize", size);
	},
	tx: {
		NUMBER(json){
			return json.toLocaleString();	
		},
		ARRAY(json){
			let html = json.map((item)=>{
				let type = (typeof item).toUpperCase();
				return APIHtml.tx[type](item);
			});
			return html;
		},
		OBJECT(json){
			if (json===null){
				return "null";
			}
			if (Array.isArray(json)){
				return APIHtml.tx.ARRAY(json);
			} else{
				const keys = Object.keys(json);
				let val = `<table class="jsont"><tr>`,
					tds =[];
				keys.forEach( (k)=>{
					val+=`<th class="key">${k}</th>`;
					tds.push( json[k] );
				});
				val+="</tr><tr>";
				tds.forEach( (td)=>{
					let type = (typeof td).toUpperCase(),
						html = APIHtml.tx[type](td);
					val+=`<td>${html}</td>`;
				});
				return `${val}</tr></table>`;
			}
		},
		STRING(json){
			return json;
		},
		BOOLEAN(json){
			return json;
		}
	}
};

const APIMap = {
	Map: null,
	map(geoPoints){
		if (APIMap.Map!==null){
			geoPoints.forEach( (geo)=>{
				const keys = Object.keys(geo.item);
				let _title = "";
				keys.forEach( (k)=>{
					_title+=`${k}: ${geo.item[k]}\n\n`;
				});
				const location = new google.maps.LatLng(geo.lat, geo.lon);
				new google.maps.Marker({
					position: location,
					map: APIMap.Map,
					title: _title
				});
			});
		}
	},
	build(results){
		const initialLocation = new google.maps.LatLng(34.0387, -118.2197),
			options = {
				zoom: 5,
				center: initialLocation,
				mapTypeId: google.maps.MapTypeId.ROADMAP
			};
		APIMap.Map = new google.maps.Map(document.getElementById("map"), options);
	}
};