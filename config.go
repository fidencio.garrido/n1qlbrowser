package main

import (
	"io/ioutil"
	"encoding/json"
	"log"
	"os"
)

const FILE_NAME = "config/config.json"

func configAddServer(server *Server){
	servers:=readConfigServer()
	if (servers != nil){
		log.Print(servers)
	}
}

func readConfigServer() []Server{
	var servers []Server
	var config []byte
	environmentConfiguration:=os.Getenv("N1QL_SERVERS")
	if (environmentConfiguration == ""){
		fcontent, err := ioutil.ReadFile(FILE_NAME)
		environmentConfiguration = string(fcontent)
		if (err==nil){
			config = fcontent
		} else {
			panic("Invalid service configuration")
			log.Print(err)
		}
	} else{
		config = []byte(environmentConfiguration)
	}
	json.Unmarshal(config, &servers)
	return servers
}

func getDataFromServer(alias string) Server{
	var sinfo Server
	servers:=readConfigServer()
	ndx:=0
	for ndx=0; ndx<len(servers); ndx++{
		if servers[ndx].Name == alias{
			sinfo = servers[ndx]
			break
		}
	}
	return sinfo
}