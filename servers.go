package main

import (
	"net/http"
	"encoding/json"
	"io/ioutil"
	"log"
	"github.com/gorilla/mux"
	"strings"
)

type Server struct {
	Name string	`json:"name"`
	Url string	`json:"url"`
	User string `json:"user"`
	Password string `json:"password"`
	Buckets []Bucket `json:"buckets"`
}

type Bucket struct{
	Bucket string	`json:"bucket"`
	Password string	`json:"password"`
}

type QueryRequest struct{
	Query string `json:"q"`
}

func createServer(w http.ResponseWriter, r *http.Request) {
	var s Server
	server, _ := ioutil.ReadAll(r.Body)
	json.Unmarshal(server, &s)
	resp, _ := json.Marshal(s)
	configAddServer(&s)
	w.Write(resp)
}

func findAllServers(w http.ResponseWriter, r *http.Request) {
	log.Print("op=findAllServers source="+r.RemoteAddr)
	servers:=readConfigServer()
	for i:=0; i<len(servers); i++{
		servers[i].User = ""
		servers[i].Password = ""
		if (servers[i].Buckets != nil){
			for j:=0; j<len(servers[i].Buckets); j++{
				servers[i].Buckets[j].Password = ""
			}
		}
	}
	json, _ := json.Marshal(servers)
	w.Write(json)
}

func connect(w http.ResponseWriter, r *http.Request) {
	alias := mux.Vars(r)["server"]
	log.Print("connect="+alias)
	server:=getDataFromServer(alias)
	buckets, _:= addConnection(server.Url, server.User, server.Password)
	resp, _ := json.Marshal(buckets)
	w.Write(resp)
}

func connectBucket(w http.ResponseWriter, r *http.Request) {
	alias := mux.Vars(r)["server"]
	bucketName := mux.Vars(r)["bucket"]
	server:=getDataFromServer(alias)
	state, err, _ := server.addBucketConnection(bucketName)
	if err == nil{
		resp, _ := json.Marshal(state)
		log.Print("op=bucketConnect bucket="+bucketName)
		w.Write(resp)
	} else{
		code:=http.StatusInternalServerError
		if strings.Contains(err.Error(), "Authentication"){
			code = http.StatusUnauthorized
		}
		http.Error(w,err.Error(),code)
	}
}

func query(w http.ResponseWriter, r *http.Request) {
	alias := mux.Vars(r)["server"]
	bucket := mux.Vars(r)["bucket"]
	server:=getDataFromServer(alias)
	var q QueryRequest
	json.NewDecoder(r.Body).Decode(&q)
	log.Print("server=" + alias + " bucket=" + bucket + " query="+q.Query )
	results, err := server.execute(bucket, q.Query)
	if err == nil {
		w.Header().Set("Content-Type", "application/json")
		j,_ := json.Marshal(results)
		w.Write(j)
	} else{
		http.Error(w,err.Error(),http.StatusInternalServerError)
	}
}