FROM alpine:3.5
MAINTAINER Fidencio Garrido <fidencio.garrido@gmail.com>
RUN mkdir /app && mkdir /app/config && mkdir /app/public
COPY n1qlBrowser /app/n1qlBrowser
COPY public/ /app/public/
VOLUME /app/config
EXPOSE 8080
WORKDIR /app
ENTRYPOINT ["./n1qlBrowser"]