package main

import (
	"flag"
	"github.com/gorilla/mux"
	"net/http"
	"time"
	"log"
)

const VERSION = "0.5.0"

var port = flag.String("p", "8080", "Service port")
var help = flag.Bool("help", false, "Displays help information")

func startService(){
	log.Println("servicePort=" + *port)
	mux := mux.NewRouter()
	publicDir := http.Dir("./public")
	mux.HandleFunc("/server", findAllServers).Methods("GET")
	mux.HandleFunc("/server", createServer).Methods("POST")
	mux.HandleFunc("/connection/{server}", connect).Methods("POST")
	mux.HandleFunc("/connection/{server}/{bucket}", connectBucket).Methods("POST")
	mux.HandleFunc("/query/{server}/{bucket}", query).Methods("POST")
	mux.PathPrefix("/").Handler(http.FileServer(publicDir))
	server := &http.Server{
		Addr: "0.0.0.0:" + *port,
		Handler: mux,
		ReadTimeout: 300 * time.Second,
	}
	server.ListenAndServe()
}

func main() {
	flag.Parse()
	if *help == true {
		println("N1QL for teams %s", VERSION)
	} else{
		log.Print("N1QL for teams ", VERSION)
		startService()
	}
}