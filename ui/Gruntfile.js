module.exports = function(grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON("package.json"),
    copy:{
      bootstrapcss: { src: "bower_components/bootstrap/dist/css/bootstrap.min.css", dest: "../public/external/bootstrap.min.css" },
      bootstrapjs: { src: "bower_components/bootstrap/dist/js/bootstrap.min.js", dest: "../public/external/bootstrap.min.js" },
      fontawesomecss: { src: "bower_components/font-awesome/css/font-awesome.min.*", dest: "../public/external/css", flatten: true, expand: true },
      fontawesomefonts: { src: "bower_components/font-awesome/fonts/*.*", dest: "../public/external/fonts", flatten: true, expand: true },
      jquery: { src: "bower_components/jquery/dist/jquery.min.*", dest: "../public/external/", flatten: true, expand: true },
      lodash: { src: "bower_components/lodash/dist/lodash.min.js", dest: "../public/external/lodash.min.js"},
      vue: { src: "bower_components/vue/dist/*.min.js", dest: "../public/external/", flatten: true, expand: true }
    },
    pug: {
    	compile:{
    		files: {
    			"../public/index.html": "views/index.pug",
    			"../public/about.html": "views/about.pug"
    		}
    	}
    },
    stylus: {
      compile: {
        options: {
          relativeDest: "../public/resources/css"
        },
        files: [
          {
            "style.css": ["css/**/*.styl"]
          }
        ]
      }
    },
    watch:{
    	ui: {
  			files: ["views/**/*.pug", "css/**/*.styl"],
  			tasks: ["pug", "stylus"]
    	}
    }
  });
  grunt.loadNpmTasks("grunt-contrib-copy");
  grunt.loadNpmTasks("grunt-contrib-pug");
  grunt.loadNpmTasks("grunt-contrib-stylus");
  grunt.loadNpmTasks("grunt-contrib-watch");
  
  grunt.registerTask("build", ["copy", "stylus", "pug"]);
  grunt.registerTask("default", ["watch"]);
};
