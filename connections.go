package main

import (
	"gopkg.in/couchbase/gocb.v1"
	"log"
	"errors"
	"time"
	"encoding/json"
)

type Connection struct{
	Server string
	Cluster *gocb.Cluster
	Manager *gocb.ClusterManager
	Buckets map[string] *gocb.Bucket
	BSettings []*gocb.BucketSettings
}

type QueryResponse struct {
	ElapsedTime time.Duration `json:"elapsedTime"`
	ExecutionTime time.Duration `json:"executionTime"`
	ResultCount uint `json:"resultCount"`
	ResultSize uint `json:"resultSize"`
	Resultset []string `json:"results"`
}

var connections = make(map[string] Connection)

func addConnection(server string, username string, password string) ([]*gocb.BucketSettings, error){
	var localError error
	var buckets []*gocb.BucketSettings
	if (connections[server].Server == server){
		log.Print("connection=recycled")
		buckets = connections[server].BSettings
	} else{
		cluster, err:=gocb.Connect(server)
		if (err!=nil){
			log.Print("connection=new status=err code=" + err.Error() + " server=" + server)
			localError = errors.New("Cannot connect to server " + server)
		} else{
			var connection Connection
			connection.Server = server
			connection.Cluster = cluster
			connections[server] = connection
			connection.Manager = cluster.Manager(username, password)
			clusterBuckets, bucketserr:=connection.Manager.GetBuckets()
			if bucketserr != nil{
				log.Print(bucketserr)
				log.Print("connection=new status=success server=" + server + " buckets=error " + " code=" +bucketserr.Error())
				localError = errors.New("Cannot get list of buckets from server " + server)
			} else{
				connection.BSettings = clusterBuckets
				buckets = connection.BSettings
				connections[server] = connection
				log.Print("connection=new status=success server=" + server)
			}
		}
	}
	return buckets, localError
}

func (server *Server) addBucketConnection(bucketName string) (bool, error, *gocb.Bucket){
	var localError error
	var bucketInfo Bucket
	isOpen := false
	ndx := 0
	var tBucket *gocb.Bucket
	for ndx=0; ndx<len(server.Buckets); ndx++ {
		if bucketName == server.Buckets[ndx].Bucket {
			bucketInfo = server.Buckets[ndx]
			break
		}
	}
	s := connections[server.Url]
	if s.Buckets[bucketName]!=nil{
		log.Print("server=" + server.Url + " openBucket="+bucketName + " newconnection=false")
	} else{
		if s.Buckets == nil{
			s.Buckets = make(map[string] *gocb.Bucket)
		}
		bucketRef, error := s.Cluster.OpenBucket(bucketName, bucketInfo.Password)
		s.Buckets[bucketName] = bucketRef
		tBucket = bucketRef
		if error != nil{
			log.Print("server=" + server.Url + " openBucket="+bucketName + " status=error newconnection=true code="+error.Error())
			localError = error
		} else{
			isOpen = true
			log.Print("server=" + server.Url + " openBucket="+bucketName + " status=success newconnection=true")
		}
	}
	return isOpen, localError, tBucket
}

func (server *Server) execute(bucketName string, query string) (QueryResponse, error){
	var localError error
	var qResults gocb.QueryResults
	var response QueryResponse
	_, err, bucket := server.addBucketConnection(bucketName)
	if err==nil{
		q:=gocb.NewN1qlQuery(query)
		results, n1qlError := bucket.ExecuteN1qlQuery(q, []interface{}{})
		qResults = results
		var row interface{}
		if n1qlError == nil{
			qResults.Close()
			for qResults.Next(&row) {
				b, _ := json.Marshal(row)
				response.Resultset = append(response.Resultset, string(b))
			}
			response.ElapsedTime = qResults.Metrics().ElapsedTime
			response.ExecutionTime = qResults.Metrics().ExecutionTime
			response.ResultCount = qResults.Metrics().ResultCount
			response.ResultSize = qResults.Metrics().ResultSize
		} else{
			message:="query_fail=" + query + " code="+n1qlError.Error()
			localError = errors.New(message)
			log.Print( message )
		}
	} else{
		message:="query_fail=" + query + " code="+err.Error()
		localError = errors.New(message)
		log.Print( message )
	}
	return response, localError
}